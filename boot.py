# Copyright [2017] [Mauro Riva <lemariva@mail.com> <lemariva.com>]
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# boot.py -- run on boot-up

import machine
import time
import uasyncio as asyncio
from aswitch import Pushbutton
import gc

relay_1 = machine.Pin(4, machine.Pin.OUT)
relay_2 = machine.Pin(5, machine.Pin.OUT)
button = machine.Pin(15, machine.Pin.IN, machine.Pin.PULL_UP)
serv_button = machine.Pin(18, machine.Pin.IN, machine.Pin.PULL_UP)
reset_button = machine.Pin(19, machine.Pin.IN, machine.Pin.PULL_UP)
led = machine.Pin(2, machine.Pin.OUT)
rtc = machine.RTC()
rtc.init((2018, 11, 1, 4, 13, 0, 0, 0))
print('rtc clock now is ', rtc.datetime())

# read the config
import ujson
config = {}

#note
#localtime(): (year, month, day, hours, minutes, seconds, weekday, yearday)
#datetime(): (year, month, day, weekday, hours, minutes, seconds, subseconds)

try:
  f = open('config.json', 'r')
  s = f.read()
  f.close()
  config = ujson.loads(s) #read config
  print('config is ', config)
except:
  print("no config found. create default")
  config['ch1_mode'] = 'off' # 0-off, 1-on, 2-on/off clock, 3-on/off interval
  config['ch1_cloop'] = 'disable' # 0- PI control loop off, 1 - PI control loop on
  config['ch1_on_interval'] = 0 # seconds
  config['ch1_off_interval'] = 0 # seconds
  config['ch1_on_clock'] = [0,0,0] # (hour, minute, second)
  config['ch1_off_clock'] = [0,0,0] # (hour, minute, second)
  config['ch1_cloop_sp']  = 10

  config['ch2_mode'] = 'off' # 0-off, 1-on, 2-on/off clock, 3-on/off interval
  config['ch2_cloop'] = 'disable' # 0- PI control loop off, 1 - PI control loop on
  config['ch2_on_interval'] = 0 # seconds
  config['ch2_off_interval'] = 0 # seconds
  config['ch2_on_clock'] = [0,0,0] # (hour, minute, second)
  config['ch2_off_clock'] = [0,0,0] # (hour, minute, second)
  config['ch2_cloop_sp']  = 11
  f1 = open('config.json', 'w')
  f1.write(ujson.dumps(config))
  f1.close()

ch1_interval_actiontime = time.time() + config['ch1_off_interval']
ch2_interval_actiontime = time.time() + config['ch2_off_interval']

async def relay_app(relay, delay):
  while True:
    print('hello with delay %d' % delay)
    relay.value(not relay.value())
    await asyncio.sleep(delay)

gc.collect()
from onewire import OneWire
from ds18x20 import DS18X20

cur_temp = 0
#temp at 21 pin
async def get_temp(pin = 21):
  dat = machine.Pin(pin)
  ds = DS18X20(OneWire(dat))
  while True:
    rom = ds.scan()
    try:
      ds.convert_temp()
      await asyncio.sleep_ms(760)
      t1 = ds.read_temp(rom[0])
      if t1 == 85.0:
        global cur_temp
        cur_temp = None
      global cur_temp
      cur_temp = t1
    except:
      global cur_temp
      cur_temp = None

gc.collect()
from pid import PID
pid_ch1 = PID(1, 0.1, 0.05, setpoint=33)
pid_ch1.output_limits = (0, 1)
pid_ch2 = PID(1, 0.1, 0.05, setpoint=33)
pid_ch2.output_limits = (0, 1)
pid_ch2.auto_mode = False
pid_ch1.auto_mode = False
ap_active = False

async def relay_1_app():
  while True:
    # if ap_active: continue
    global rtc
    global ch1_interval_actiontime
    ch1_mode = config['ch1_mode']
    if ch1_mode == 'off':  #constant off
      relay_1.value(0)
    elif ch1_mode == 'on':  #constant on
      if config['ch1_cloop'] == 'disable': relay_1.value(1)
      else:
        if cur_temp: relay_1.value(int(pid_ch1(cur_temp)))


    elif ch1_mode == 'clock':   # clock mode
      if rtc.datetime()[4:7] == tuple(config['ch1_on_clock']):
        if relay_1.value() == True: continue
        print('ch1 is on (clock)')
        if config['ch1_cloop'] == 'disable': relay_1.value(1)
        else:
          if cur_temp: relay_1.value(int(pid_ch1(cur_temp)))
      elif rtc.datetime()[4:7] == tuple(config['ch1_off_clock']):
        if relay_1.value() == False : continue
        print('ch1 is off (clock)')
        relay_1.value(0)
      else:
        pass
    elif ch1_mode == 'interval':   # interval mode
      if relay_1.value():
        if (time.time() - ch1_interval_actiontime) >= 0:
          relay_1.value(0)
          ch1_interval_actiontime = time.time() + config['ch1_off_interval']
          print('ch1 is off (interval)')
      else:
        if (time.time() - ch1_interval_actiontime) >= 0:
          print('ch1 is on (interval)')
          ch1_interval_actiontime = time.time() + config['ch1_on_interval']
          if config['ch1_cloop'] == 'disable': relay_1.value(1)
          else:
            if cur_temp: relay_1.value(int(pid_ch1(cur_temp)))
    else:
      print("unknown mode")
    await asyncio.sleep(0)

ch2_on = False

async def relay_2_app():
  while True:
    # if ap_active: continue
    global rtc
    global ch2_interval_actiontime
    ch2_mode = config['ch2_mode']
    global ch2_on
    if ch2_mode == 'off':  #constant off
      relay_2.value(0)
      if pid_ch2.auto_mode: pid_ch2.auto_mode = False

    elif ch2_mode == 'on':  #constant on
      if config['ch2_cloop'] == 'disable':
        relay_2.value(1)
        if pid_ch2.auto_mode: pid_ch2.auto_mode = False
      else:
        if not pid_ch2.auto_mode: pid_ch2.auto_mode = True
        if cur_temp: relay_2.value(int(pid_ch2(cur_temp)))

    elif ch2_mode == 'clock':   # clock mode
      if rtc.datetime()[4:7] == tuple(config['ch2_on_clock']):
        if not ch2_on: ch2_on = True
        # if ch2_on == True: continue
        # print('ch2 is on (clock)')
        if config['ch2_cloop'] == 'disable': relay_2.value(1)
        else:
          if cur_temp: relay_2.value(int(pid_ch2(cur_temp)))

      elif rtc.datetime()[4:7] == tuple(config['ch2_off_clock']):
        if relay_2.value() == False : continue
        print('ch2 is off (clock)')
        relay_2.value(0)
      else:
        pass

    elif ch2_mode == 'interval':   # interval mode
      if relay_2.value():
        if (time.time() - ch2_interval_actiontime) >= 0:
          print('ch2 is off (interval)')
          relay_2.value(0)
          ch2_interval_actiontime = time.time() + config['ch2_off_interval']
          if pid_ch2.auto_mode: pid_ch2.auto_mode = False
      else:
        if (time.time() - ch2_interval_actiontime) >= 0:
          print('ch2 is on (interval)')
          ch2_interval_actiontime = time.time() + config['ch2_on_interval']
          if config['ch2_cloop'] == 'disable': relay_2.value(1)
          else:
            if cur_temp: relay_2.value(int(pid_ch2(cur_temp)))
    else:
      print("unknown mode")
    await asyncio.sleep(0)

async def main():
  sec = 0
  while True:
    if not sec == rtc.datetime()[6]:
      print(rtc.datetime(), ': t - ', cur_temp)
      sec = rtc.datetime()[6]
    await asyncio.sleep(0)

gc.collect()
import captive_portal.__main__

def exit_app():
  print('now exit')
  exit()

def reset_cfg():
  gc.collect()
  import os
  print('remove config.json')
  os.remove('config.json')

def double_click():
  print('double click')
  gc.collect()
  access_point_name = 'PhytoShell'
  ap_if = None
  import network

  ap_if = network.WLAN(network.AP_IF)
  if not ap_if.active():
      led.value(1)
      print("turn AP on")
      ap_if.active(True)
      global ap_active
      ap_active = True
      ap_if.config(essid=access_point_name, authmode=network.AUTH_OPEN)
      ap_if.ifconfig(('192.168.4.1', '255.255.255.0', '192.168.4.1', '192.168.4.1'))
      captive_portal.__main__.main(host=ap_if.ifconfig()[0], port=80)
  else:
      ap_if.active(False)
      led.value(0)
      global ap_active
      ap_active = False
      try:
        f = open('config.json', 'r')
        s = f.read()
        f.close()
        global config
        config = ujson.loads(s) #read config
        print('new config is ', config)
      except:
        print('cannot read config')


pb = Pushbutton(button)
pb_serv = Pushbutton(serv_button)
pb_reset = Pushbutton(reset_button)
pb_serv.long_func(exit_app, )
pb_reset.long_func(reset_cfg, )
pb.double_func(double_click, )
loop = asyncio.get_event_loop()
loop.create_task(relay_1_app())
loop.create_task(relay_2_app())
loop.create_task(get_temp())
loop.run_until_complete(main())

#connectivitycheck.gstatic.com

# >>> from onewire import OneWire
# >>> from ds18x20 import DS18X20
# >>>
# >>>
# >>>
# >>> dat = machine.Pin(21)
# >>> ds = DS18X20(OneWire(dat))
# >>> rom = ds.scan()
# >>> rom
# [bytearray(b'(\xff+X\x93\x16\x04\xef')]
# >>> ds.convert_temp()
# >>> ds.read_temp(rom[0])
# 85.0
